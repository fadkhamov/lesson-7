const start = document.getElementById('start');
const pause = document.getElementById('pause');
const reset = document.getElementById('reset');
let minutes = document.getElementById('minutes');
let seconds = document.getElementById('seconds');

let setMinutes = minutes.innerHTML;
let setSeconds = seconds.innerHTML;


let interval;

reset.addEventListener('click', function(){
    clearInterval(interval)
    setMinutes = 20;
    setSeconds = '00';
    minutes.innerHTML = setMinutes;
    seconds.innerHTML = setSeconds;
})

start.addEventListener('click', function(){
    interval = setInterval(() => {
        if(setSeconds == 0){
            setMinutes -= 1;
            minutes.innerHTML = setMinutes;
            setSeconds = 60;
        }
        setSeconds--;
        seconds.innerHTML = setSeconds;
    }, 1000)
});

pause.addEventListener('click', function(){
    clearInterval(interval)
})